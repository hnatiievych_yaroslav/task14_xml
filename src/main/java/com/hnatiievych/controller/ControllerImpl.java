package com.hnatiievych.controller;


import com.hnatiievych.model.parser.Parser;

public class ControllerImpl implements Controller {
    private Parser parser;


    public ControllerImpl(){
        parser=new Parser();
    }

    @Override
    public void parseDOM() {
        System.out.println(parser.domParser());
    }

    @Override
    public void parseSax() {
        System.out.println(parser.saxParser());
    }

    @Override
    public void parseStax() {
        System.out.println(parser.staxParser());
    }

    @Override
    public void sortedListToXML() {
        parser.ObjectToXML(parser.sortBanks(parser.domParser()));
    }
}

