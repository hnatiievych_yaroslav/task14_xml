package com.hnatiievych.controller;

public interface Controller {
        void parseDOM();
        void parseSax();
        void parseStax();
        void sortedListToXML();
    }
