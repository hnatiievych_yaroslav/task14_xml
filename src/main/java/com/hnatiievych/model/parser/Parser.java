package com.hnatiievych.model.parser;

import com.hnatiievych.model.Bank;
import com.hnatiievych.model.BankComparator;
import com.hnatiievych.model.Banks;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class Parser {
    private static String xml = "src/main/resources/BanksXML.xml";
    private static String xsd = "src/main/resources/BanksXSD.xsd";
    private static File xmlFile = new File(xml);
    private static File xsdFile = new File(xsd);
    private List<Bank> banks;

    public List<Bank> domParser() {
        if (!XMLValidation.validateXMLSchema(xsd, xml)) {
            throw new RuntimeException("validation failed");
        }
        return DomParserBank.parserXmlDom(xmlFile);
    }

    public List<Bank> saxParser() {
        if (!XMLValidation.validateXMLSchema(xsd, xml)) {
            throw new RuntimeException("validation failed");
        }
        try {
            SAXParserFactory parserFactor = SAXParserFactory.newInstance();
            SAXParser parser = parserFactor.newSAXParser();
            SaxParserBank handler = new SaxParserBank();
            parser.parse(xmlFile, handler);
            banks = handler.getBankList();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return banks;
    }


    public List<Bank> staxParser() {
        if (!XMLValidation.validateXMLSchema(xsd, xml)) {
            throw new RuntimeException("validation failed");
        }
        return StaxParser.parserXmlStax(xmlFile);
    }

    public void ObjectToXML(List<Bank> banks) {
        try {
            Banks banksList = new Banks();
            banksList.setBanks(banks);
            JAXBContext jaxbContext = JAXBContext.newInstance(Banks.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            //Marshal the employees list in file
            jaxbMarshaller.marshal(banksList, new File("src/main/resources/sortedBank.xml"));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public List<Bank> sortBanks(List<Bank>banks){
        if(!banks.isEmpty()){
            banks.sort(new BankComparator());
        }else{
            System.out.println("Please first parse xml file!!");
        }
        return banks;

    }



}