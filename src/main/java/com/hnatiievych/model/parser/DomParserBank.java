package com.hnatiievych.model.parser;

import com.hnatiievych.model.Bank;
import com.hnatiievych.model.Type;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DomParserBank {
    public static List<Bank> parserXmlDom (File xml){
        List<Bank> banks = new ArrayList<Bank>();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(xml);
            document.getDocumentElement().normalize();
            NodeList nList = document.getElementsByTagName("bank");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node node = nList.item(temp);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) node;
                    Bank bank = new Bank();
                    bank.setId(Integer.parseInt(eElement.getElementsByTagName("id").item(0).getTextContent()));
                    bank.setName(eElement.getElementsByTagName("name").item(0).getTextContent());
                    bank.setCountry(eElement.getElementsByTagName("country").item(0).getTextContent());
                    bank.setType(parseTypeEnum(eElement.getElementsByTagName("type").item(0).getTextContent()));
                    bank.setDepositor(eElement.getElementsByTagName("depositor").item(0).getTextContent());
                    bank.setAccountId(Long.parseLong(eElement.getElementsByTagName("accountId").item(0).getTextContent()));
                    bank.setAmountOnDeposit(Long.parseLong(eElement.getElementsByTagName("amountOnDeposit").item(0).getTextContent()));
                    bank.setProfitability(Integer.parseInt(eElement.getElementsByTagName("profitability").item(0).getTextContent()));
                    bank.setTimeConstraints(Integer.parseInt(eElement.getElementsByTagName("timeConstraints").item(0).getTextContent()));
                    banks.add(bank);
                }
            }
        }catch (ParserConfigurationException | SAXException| IOException e){
            e.printStackTrace();
        }

        return banks;
    }
    private static Type parseTypeEnum(String str){
            return Arrays.stream(Type.values())
                    .filter(e -> e.toString().toLowerCase().equals(str))
                    .findFirst()
                    .orElse(null);
    }
}
