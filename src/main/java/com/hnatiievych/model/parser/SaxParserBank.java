package com.hnatiievych.model.parser;

import com.hnatiievych.model.Bank;
import com.hnatiievych.model.Type;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SaxParserBank extends DefaultHandler {
    private static Bank bank = null;
    private static List<Bank> bankList = new ArrayList<>();
    private static String text = null;

    @Override
    // A start tag is encountered.
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {
        switch (qName) {
            case "bank": {
                bank = new Bank();
                break;
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case "bank": {
                bankList.add(bank);
                break;
            }
            case "id": {
                bank.setId(Integer.parseInt(text));
                break;
            }
            case "name": {
                bank.setName(text);
                break;
            }
            case "country": {
                bank.setCountry(text);
                break;
            }
            case "type": {
                bank.setType(parseTypeEnum(text));
                break;
            }
            case "depositor": {
                bank.setDepositor(text);
                break;
            }
            case "accountId": {
                bank.setAccountId(Long.parseLong(text));
                break;
            }
            case "amountOnDeposit": {
                bank.setAmountOnDeposit(Long.parseLong(text));
                break;
            }
            case "profitability": {
                bank.setProfitability(Integer.parseInt(text));
                break;
            }
            case "timeConstraints": {
                bank.setTimeConstraints(Integer.parseInt(text));
                break;
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        text = String.copyValueOf(ch, start, length).trim();
    }

    private static Type parseTypeEnum(String str){
        return Arrays.stream(Type.values())
                .filter(e -> e.toString().toLowerCase().equals(str))
                .findFirst()
                .orElse(null);
    }

    public List<Bank> getBankList(){
        return this.getBankList();
    }
}
