package com.hnatiievych.model.parser;

import com.hnatiievych.model.Bank;
import com.hnatiievych.model.Type;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StaxParser {

    public static List<Bank> parserXmlStax(File xml) {
        List<Bank> bankList = null;
        Bank bank = null;
        String text = null;
        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLStreamReader reader = factory.createXMLStreamReader(new FileInputStream(xml));

            while (reader.hasNext()) {
                int Event = reader.next();

                switch (Event) {
                    case XMLStreamConstants.START_ELEMENT: {
                        if ("bank".equals(reader.getLocalName())) {
                            bank = new Bank();
                        }
                        if ("banks".equals(reader.getLocalName()))
                            bankList = new ArrayList<>();
                        break;
                    }
                    case XMLStreamConstants.CHARACTERS: {
                        text = reader.getText().trim();
                        break;
                    }
                    case XMLStreamConstants.END_ELEMENT: {
                        switch (reader.getLocalName()) {
                            case "bank": {
                                bankList.add(bank);
                                break;
                            }
                            case "id": {
                                bank.setId(Integer.parseInt(text));
                                break;
                            }
                            case "name": {
                                bank.setName(text);
                                break;
                            }
                            case "country": {
                                bank.setCountry(text);
                                break;
                            }
                            case "type": {
                                bank.setType(parseTypeEnum(text));
                                break;
                            }
                            case "depositor": {
                                bank.setDepositor(text);
                                break;
                            }
                            case "accountId": {
                                bank.setAccountId(Long.parseLong(text));
                                break;
                            }
                            case "amountOnDeposit": {
                                bank.setAmountOnDeposit(Long.parseLong(text));
                                break;
                            }
                            case "profitability": {
                                bank.setProfitability(Integer.parseInt(text));
                                break;
                            }
                            case "timeConstraints": {
                                bank.setTimeConstraints(Integer.parseInt(text));
                                break;
                            }
                        }
                    }
                }
            }
        } catch (FileNotFoundException | XMLStreamException e) {
            e.printStackTrace();
        }
        return bankList;
    }

    private static Type parseTypeEnum(String str) {
        return Arrays.stream(Type.values())
                .filter(e -> e.toString().toLowerCase().equals(str))
                .findFirst()
                .orElse(null);
    }
}
