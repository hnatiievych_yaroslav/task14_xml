<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <h2>Banks</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>ID</th>
                        <th>name</th>
                        <th>country</th>
                        <th>type</th>
                        <th>depositor</th>
                        <th>account Id</th>
                        <th>amount on deposit</th>
                        <th>profitability</th>
                        <th>time constraints</th>
                    </tr>
                    <xsl:for-each select="banks/bank">
                        <tr>
                            <td><xsl:value-of select="id"/></td>
                            <td><xsl:value-of select="name"/></td>
                            <td><xsl:value-of select="country"/></td>
                            <td><xsl:value-of select="type"/></td>
                            <td><xsl:value-of select="depositor"/></td>
                            <td><xsl:value-of select="accountId"/></td>
                            <td><xsl:value-of select="amountOnDeposit"/></td>
                            <td><xsl:value-of select="profitability"/></td>
                            <td><xsl:value-of select="timeConstraints"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>